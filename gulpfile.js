var gulp = require("gulp");
var sonarqubeScanner = require("sonarqube-scanner");
var argv = require("yargs").argv;
gulp.task("sonar", function (callback) {
  sonarqubeScanner(
    {
      serverUrl: argv.sonarServerUrl,
      token: argv.sonarLoginToken,
      options: {
        "sonar.projectKey": argv.sonarProjectKey,
        "sonar.sourceEncoding": "utf-8",
        "sonar.projectVersion": argv.sonarProjectVersion
      },
    },
    callback
  );
});
