pipeline {
    agent any

    tools {
        nodejs 'nodejs-10.x'
    }
    
    options {
	    buildDiscarder(logRotator(numToKeepStr:'7', daysToKeepStr:'7'))
	    disableConcurrentBuilds()
	}

    stages {
        stage('Install') {
            steps {
            	sh 'npm install'
            }
        }
        stage('Build') {
            steps {
                sh 'npm run build'
            }
        }
		stage('Test') {
            steps {
				sh 'npm test'
            }
			post {
                always {
                    junit 'junit.xml'
                }
                success {
                    script {
        	    		def summary = junit testResults: 'junit.xml'
	                    slackSend (
	                       channel: "#hk-asl-cims-jenkins",
	                       color: '#00FF00',
	                       message: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})\n *Test Summary* - Total: ${summary.totalCount}, Failures: ${summary.failCount}, Skipped: ${summary.skipCount}, Passed: ${summary.passCount}"
	                    )
        	    	}  
        	    }	
        	    failure {
        	    	script {
        	    		def summary = junit testResults: 'junit.xml'
	                    slackSend (
	                       channel: "#hk-asl-cims-jenkins",
	                       color: '#FF0000',
	                       message: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})\n *Test Summary* - Total: ${summary.totalCount}, Failures: ${summary.failCount}, Skipped: ${summary.skipCount}, Passed: ${summary.passCount}"
	                    )
        	    	}        	        
        	    }
            }
        }
        stage('Code Scan') {
            steps {
            	sh '''#!/bin/bash
            		npm install -g gulp
		        	SONARQUTE_PROJECT_VERSION=$(cat package.json   | grep version   | head -1   | awk -F: '{ print $2 }'   | sed 's/[//",]//g'   | tr -d '[[:space:]]')
					echo $SONARQUTE_PROJECT_VERSION
					gulp sonar --sonarServerUrl=$CI_SONAR_HOST_URL --sonarLoginToken=$CI_SONAR_LOGIN_TOKEN --sonarProjectKey="docker-create-react-app" --sonarProjectVersion=$SONARQUTE_PROJECT_VERSION
		        '''
            }
        }
        stage('Build Docker Image') {
            steps {
                script {
                    docker.build('react-app:latest')
                }
            }
        }
        stage('Start Docker Image') {
            steps {
                sh 'docker-compose -f docker-compose.yml down --remove-orphans'
                sh 'docker-compose -f docker-compose.yml rm'
                sh 'docker-compose -f docker-compose.yml up -d'
            }
        }
    }
    post {
	    success {
	    	slackSend (color: '#00FF00', message: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})", channel: '#hk-asl-cims-jenkins')
	    }	
	    failure {
	    	slackSend (color: '#FF0000', message: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})", channel: '#hk-asl-cims-jenkins')
	    }
	}
}
